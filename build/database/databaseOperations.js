var constants = require("../constants");
var pg = require('pg');

class DatabaseOperations
{
    constructor(dbinfo)
    {
        this.dbInfo = dbinfo;
        this.pool = this.createPool();
    }

    createPool()
    {
        return new pg.Pool({
            user: this.dbInfo.user,
            host: this.dbInfo.host,
            database: this.dbInfo.database,
            password: this.dbInfo.password,
            port: this.dbInfo.port});
    }

    async initialize()
    {
        console.log("Checking if database already exists...");
        let dbExists = await this.tablesExist()
        if(!dbExists)
        {
            console.log("Database doesn't exist. Creating database:\nDropping existing tables...");
            await this.dropTables();
            console.log("Any existing tables dropped, Now creating new tables...");
            await this.createTables();
            console.log("Database creation complete! Checking if database now exists...");
            dbExists = await this.tablesExist();
            if(dbExists)
            {
                console.log("Database exists! Successfully initialized.");
            }
            else
            {
                console.log("Database doesn't exist. Failed to create.");
            }
        }    
        else
        {
            console.log("Database exists and is now initialized.");
        }
    }

    async tablesExist()
    {
        let allTablesExist = true;
        let tablesNames = this.dbInfo.tables;
        for(let ii=0; ii<tablesNames.length; ii++)
        {
            let exists = await this.tableExists(tablesNames[ii], this.pool);
            let msg = " table exists.";
            if(!exists)
            {
                msg = " table doesn't exist.";
                allTablesExist = false;
            }
            console.log(tablesNames[ii] + msg);
        }
        return allTablesExist;
    }

    async tableExists(tablename)
    {
        let sql = "SELECT EXISTS ( SELECT * FROM information_schema.tables WHERE  table_schema = 'public' AND table_name = '" + tablename + "');";
        let exists;
        
        try
        {
            let res = await this.pool.query(sql);
            exists = res.rows[0].exists;
        } catch (err)
        {
            console.log("error checking if table exists (" + tablename + ")");
            console.log(err);
        }

        return exists;
    }    

    //If NoTables is true at end of function, then non of tables in dbInfo.tables exist in db. 
    //If atleast one table not droppable then NoTables will be false. (Non existent tables wont make NoTables==false)
    async dropTables()
    {
        let NoTables = true;
        for(let ii=this.dbInfo.tables.length-1; ii>=0; ii--)
        {
            if(!await this.dropTable(this.dbInfo.tables[ii], this.pool))
            {
                NoTables = false;
            }
        };
    }

    async dropTable(tableName)
    {
        let sql =  'DROP TABLE public."' + tableName + '"';
        let res;
        let err;
        let NoTable;
        try
        {
            res = await this.pool.query(sql);
        }
        catch(e)
        {
            err = e;
        }
        
        if(res)
        {
            console.log(tableName + " table dropped successfully")
            NoTable = true;
        }
        else if(err.routine && err.routine == 'DropErrorMsgNonExistent')
        {
            console.log(tableName + " table doesn't exist... skipping");            
            NoTable = true;
        }
        else
        {
            console.log("Unable to drop table : ");
            console.log(err);
            NoTable = false;
        }

        return NoTable;
    }

    async createTables()
    {
        let tables = this.dbInfo.tables;
        
        let success;
        for(let ii = 0; ii<4; ii++)
        {
            console.log("Creating table " + tables[ii]);
            success = await this.createTable(tables[ii]);
            success?console.log(tables[ii] + " creation success"):console.log(tables[ii] + " creation failure");
        }
    }

    async createTable(tableName)
    {
        let created;
        try
        {
            let res = await this.pool.query(this.dbInfo.tablesCreationSQL[tableName]);
            created = true;
        }
        catch(err)
        {
            created = false;
        }
        return created;
    }
}

module.exports = {databaseOperations : DatabaseOperations};