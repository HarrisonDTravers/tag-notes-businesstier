var constants = require("../constants");
var pg = require('pg');
var format = require('../utilities');

class TableOperations
{
    constructor(dbinfo)
    {
        this.dbInfo = dbinfo;
        this.pool = this.createPool();
    }

    createPool()
    {
        return new pg.Pool({
            user: this.dbInfo.user,
            host: this.dbInfo.host,
            database: this.dbInfo.database,
            password: this.dbInfo.password,
            port: this.dbInfo.port});
    }    

    async insertIntoTable(tableName, values)
    {
        let res;
        let err;

        try
        {
            let sql = this.dbInfo.tablesInsertionSQL[tableName];
            sql = format(sql, values);
            res = await this.pool.query(sql);
        } catch (e)
        {
            throw new Error(e);
        }

        return res?res:err;
    }

    async selectAllWithIdFromTable(tableName, values)
    {
        let res;
        let err;       
        let sql;

        try
        {            
            console.log("Select : tablename : " + tableName + " values : " + values);            
            sql = this.dbInfo.tablesSelectByColumnSQL[tableName];
            console.log(tableName + " : " + sql);     
            sql = format(sql, values);

            console.log(sql);
            res = await this.pool.query(sql);
            console.log(res);
        } catch (e)
        {
            err = e;
            console.log(e);
        }
        return res?res:err;
    }

    async selectRowFromTable(tableName, values)
    {
        let res;
        let err;

        try
        {            
            let sql = this.dbInfo.tablesSelectSQL[tableName];
            sql = format(sql, values);
            res = await this.pool.query(sql);
        } catch (e)
        {
            err = e;
        }

        return res?res:err;
    }

    async updateInTable(tableName, values)
    {
        let res;
        let err;

        try
        {
            let sql = this.dbInfo.tablesUpdateSQL[tableName];
            sql = format(sql, values);
            res = await this.pool.query(sql);
        } catch (e)
        {
            err = e;
        }

        console.log("TEST TEST TEST" + res, err);
    
        return res?res:err;
    }

    async deleteFromTable(tableName, values)
    {
        let res;
        let err;

        try
        {
            let sql = this.dbInfo.tablesDeleteSQL[tableName];
            sql = format(sql, values);
            res = await this.pool.query(sql);
        } catch (e)
        {
            err = e;
        }

        return res?res:err;
    }
}

module.exports = {tableOperations : TableOperations}