var constants = require("../constants");
var dbOps = require('./databaseOperations');
var tOps = require('./tableOperations');
var readline = require('readline-sync');
var LoremIpsum = require("lorem-ipsum").LoremIpsum;

//didnt want to make utilities.js just for this
function makeid(length) {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

async function generate()
{
    let setName = "generatedSet";
    console.log(await tOpsObj.insertIntoTable("sets", [setName]));
    await randomNotes(setName);
    await randomTags(setName);
    await randomTagsOnNotes(setName);
}

async function randomNotes(setName)
{
    let lorem = new LoremIpsum();
    let images = [
        'https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
        'https://images.unsplash.com/photo-1532386236358-a33d8a9434e3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1278&q=80',
        'https://images.unsplash.com/photo-1517331156700-3c241d2b4d83?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1348&q=80',
        'https://images.unsplash.com/photo-1548546738-8509cb246ed3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
        'https://images.unsplash.com/photo-1503844281047-cf42eade5ca5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1355&q=80',
        'https://images.unsplash.com/photo-1488740304459-45c4277e7daf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
        'https://images.unsplash.com/photo-1519052537078-e6302a4968d4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80',
        'https://images.unsplash.com/photo-1514231535980-340519b141a9?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80',
        'https://images.unsplash.com/photo-1548802673-380ab8ebc7b7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=675&q=80',
        'https://images.unsplash.com/photo-1494256997604-768d1f608cac?ixlib=rb-1.2.1&auto=format&fit=crop&w=1401&q=80'
    ]
    let minRows = 30;
    let numRows = Math.floor((Math.random() * (maxRows-minRows)) + minRows);
    for(var ii=0; ii<numRows; ii++)
    {   
        let dataType = Math.floor(Math.random() * 2);
        let dataObj;
        switch(dataType)
        {
            case 0:
            {                
                dataType = "string";
                dataObj = lorem.generateSentences(1);
            } break;
            case 1:
            {
                dataType = "image";
                dataObj = images[Math.floor(Math.random() * (images.length-1))];
            } break;
        }
        let values = [dataObj, dataType, setName];
        try
        {
            await tOpsObj.insertIntoTable("notes", values);
        }
        catch(err){throw new Error(err)}
        console.log("Created Note : " + values);
    }
}

async function randomTags(setName)
{
    let lorem = new LoremIpsum();
    let hasSuperTag;// = Math.random() < 0.1; //10% chance of having super tag
    let maxRows = 200;
    let minRows = 60;    
    let numRows = Math.floor((Math.random() * (maxRows-minRows)) + minRows);
    for(var ii=0; ii<numRows; ii++)
    {   
        let tagName = lorem.generateWords(1);
        let superTagId = hasSuperTag&&ii!=0?Math.floor(Math.random() * ii):null; //Can only have super tags that currently exist, hence ii.
        let values = [tagName, superTagId, setName];
        try
        {
        await tOpsObj.insertIntoTable("tags", values);
        }
        catch(err){throw new Error(err)}
        console.log("Created Tag : " + values);
    }
}

async function randomTagsOnNotes(setName)
{
    let notes = await tOpsObj.selectAllWithIdFromTable("notes", ["generatedSet"]);
    let tags = await tOpsObj.selectAllWithIdFromTable("tags", ["generatedSet"]);
    let tagChance = 0.25;
    for(let ii=0; ii<notes.rows.length; ii++)
    {
        while(Math.random() < tagChance)
        {
            let tagRowNum = Math.floor(Math.random() * tags.rows.length);
            console.log(tagRowNum);
            let selectedTag = tags.rows[tagRowNum];
            try
            {
                await tOpsObj.insertIntoTable("tagsonnotes", [selectedTag.tagid, notes.rows[ii].noteid]);
                console.log("Created Tag " + selectedTag.tagname + " on Note with id " + notes.rows[ii].noteid);
            }
            catch(err){console.log(err);}
        }
    }
    /*notes.ForEach(async (note) => {
        while(Math.random() < tagChance)
        {
            let selectedTag = tags[Math.floor(Math.random() * tags.length)];
            await tOpsObj.insertIntoTable("tagsOnNote", [selectedTag.tagid, note.noteid])
            console.log("Created Tag " + selectedTag.tagName + " on Note with id " + note.noteid);
        }
    });*/
}

async function runner()
{
    let tableNames = dbConstants.tables;
    let res;
    let quit = false;
    const prompt = "Possible args :\n" +
                 " initialize\n" + 
                 " drop\n" + 
                 " rebuild\n" + 
                 " generate\n" +
                 " quit\n" + 
                 " insert +\n" + 
                 "  <set name> into <table name>\n" + 
                 "  <tag id> <note id> into <table name>\n" + 
                 "  <note data> <data type> <set name> into <table name>\n" + 
                 "  <tag name> <super tag id> <set name> into <table name>\n" + 
                 " select +\n" + 
                 "  <primary key> from <table name>\n"
                 " update +\n" +
                 "  <table name> row <set name> values <set name>\n" + 
                 "  <table name> row <note id> values <note data> <note type> <set name>\n" + 
                 "  <table name> row <tag id> values <tag name> <super tag id> <set name>\n" + 
                 "  <table name> row <tag id> <note id> values <tag id> <note id>\n" + 
                 " delete +\n" +
                 "  <key> from <table name>\n";

    while(!quit)
    {   
        console.log(prompt);
        let choice = readline.question("args : ");
        let choiceArgs = choice.split(" ");
        if (choiceArgs.length == 1)
        {
            switch(choiceArgs[0])
            {
                case "initialize":
                    await dbOpsObj.initialize();
                    break;
                case "drop":
                    await dbOpsObj.dropTables();
                    break;
                case "generate":                
                    await dbOpsObj.dropTables();
                    await dbOpsObj.createTables();
                    await generate();                    
                    break;
                case "rebuild":
                    await dbOpsObj.dropTables().then(() => {dbOpsObj.createTables();})            
                    break;
                case "quit":
                    quit = true;
                    break;
            }            
        }
        else if (choiceArgs[0] == "insert")
        {
            switch(choiceArgs.length)
            {
                case 4:
                    switch(choiceArgs[3])
                    {
                        case (tableNames[0]):
                            let setName = choiceArgs[1];
                            console.log("inserting values (" + setName + ") into table (" + choiceArgs[3] + ")")
                            res = await tOpsObj.insertIntoTable(choiceArgs[3], [setName]);
                            break;
                        default:
                            console.log(choiceArgs[3] + " table does not exist.");
                            break;
                    }
                    break;
                case 7:
                    switch(choiceArgs[2])
                    {
                        case (tableNames[3]):
                            let tagid = choiceArgs[1];
                            let noteid = choiceArgs[2];
                            console.log("inserting values (" + tagid + ", " + noteid + ") into table (" + choiceArgs[2] + ")")
                            res = await tOpsObj.insertIntoTable(choiceArgs[2], [tagid, noteid]);
                            break;
                        default:
                            console.log(choiceArgs[4] + " table does not exist.");
                            break;
                    }
                    break;
                case 6:
                    switch(choiceArgs[5])
                    {        
                        case (tableNames[1]):
                            let noteData = choiceArgs[1];
                            let dataType = choiceArgs[2];
                            let setName = choiceArgs[3];
                            console.log("inserting values (" + noteData  + ", " + dataType  + ", " + setName + ") into table (" + choiceArgs[5] + ")");
                            try
                            {
                                res = await tOpsObj.insertIntoTable(choiceArgs[5], [noteData, dataType, setName]);
                            }
                            catch(err)
                            {
                                console.log(err);
                            }
                            break;
                        case (dbConstants.tables[2]):
                            let tagName = choiceArgs[1];
                            let superTagId; choiceArgs[2]=="null"?superTagId=null:superTagId=choiceArgs[2];
                            let targetSet = choiceArgs[3];
                            console.log("inserting values (" + tagName  + ", " + superTagId  + ", " + setName + ") into table (" + choiceArgs[5] + ")");
                            try
                            {
                                res = await tOpsObj.insertIntoTable(choiceArgs[5], [tagName, superTagId, setName]);
                            }
                            catch(err)
                            {
                                console.log(err);
                            }
                            break;
                        default:
                            console.log(choiceArgs[5] + " table does not exist.");
                            break;
                    }
            }
            if(res.name == 'error')
            {
                console.log("Unable to insert row. Detail : " + res.detail);
            }
            else
            {
                console.log("Successfully inserted row.");
            }
        }
        else if(choiceArgs[0] == "select")
        {
            switch(choiceArgs.length)
            {
                case 4:
                    let key = choiceArgs[1];
                    console.log("select row with id (" + key + ") from table (" + choiceArgs[3] + ")");
                    res = await tOpsObj.selectAllFromTable(choiceArgs[3], [key]);
                    break;
            }
            
            if(res.name == 'error')
            {
                if(res.detail)
                    console.log("Unable to select row. Detail : " + res.detail);
                else
                    console.log("Unable to select row. Error Object : " + res);
            }
            else
            {
                console.log("Successfully selected data :" + res);
            }
        }
        else if(choiceArgs[0] == "update")
        {
            switch(choiceArgs.length)
            {
                case 6:
                    switch(choiceArgs[1])
                    {
                        case (tableNames[0]):
                            let tableName = choiceArgs[1];
                            let id = choiceArgs[3];
                            let values = choiceArgs[5];
                            console.log("Updating a row in " + tableName + " with id " + id + " with values (" + values + ")");
                            res = await tOpsObj.updateInTable(tableName, [values, id]);
                            break;
                        default:
                            console.log(choiceArgs[1] + " table does not exist.");
                            break;
                    }
                    break;
                case 7:
                    switch(choiceArgs[1])
                    {
                        case (tableNames[3]):
                            let tagid = choiceArgs[1];
                            let noteid = choiceArgs[2];
                            console.log("inserting values (" + tagid + ", " + noteid + ") into table (" + choiceArgs[2] + ")")
                            res = await tOpsObj.insertIntoTable(choiceArgs[2], [tagid, noteid]);
                            break;
                        default:
                            console.log(choiceArgs[4] + " table does not exist.");
                            break;
                    }
                    break;
                case 8:
                    switch(choiceArgs[1])
                    {        
                        case (tableNames[1]):
                            let noteData = choiceArgs[1];
                            let dataType = choiceArgs[2];
                            let setName = choiceArgs[3];
                            console.log("inserting values (" + noteData  + ", " + dataType  + ", " + setName + ") into table (" + choiceArgs[3] + ")");
                            try
                            {
                                res = await tOpsObj.insertIntoTable(choiceArgs[3], [noteData, dataType, setName]);
                            }
                            catch(err)
                            {
                                console.log(err);
                            }
                            break;
                        case (dbConstants.tables[2]):
                            let tagName = choiceArgs[1];
                            let superTagId; choiceArgs[2]=="null"?superTagId=null:superTagId=choiceArgs[2];
                            let targetSet = choiceArgs[3];
                            console.log("inserting values (" + tagName  + ", " + superTagId  + ", " + targetSet + ") into table (" + choiceArgs[5] + ")");
                            try
                            {
                                res = await tOpsObj.insertIntoTable(choiceArgs[5], [tagName, superTagId, setName]);
                            }
                            catch(err)
                            {
                                console.log(err);
                            }
                            break;
                        default:
                            console.log(choiceArgs[5] + " table does not exist.");
                            break;
                    }
                default:
                console.log("No arguments exist for update of length : " + choiceArgs.length);
                break;
            }     
            if(res.name)
            {
                if(res.name  == 'error')
                    console.log("Unable to insert row. Detail : " + res.detail);
            }
            else
            {
                console.log("Successfully inserted row.");
                console.log(res);
            }       
        }
        else if(choiceArgs[0] == "delete")
        {
            switch(choiceArgs.length)
            {
                case 4:
                    let key = choiceArgs[1];
                    console.log("deleting row (" + key + ") from table (" + choiceArgs[3] + ")")
                    res = await tOpsObj.deleteFromTable(choiceArgs[3], [key]);
                    break;
            }
            
            if(res.name == 'error')
            {
                if(res.detail)
                    console.log("Unable to delete row. Detail : " + res.detail);
                else
                    console.log("Unable to delete row. Error Object : " + res);
            }
            else
            {
                console.log("Successfully deleted data.");
            }
        }
        else
        {
            console.log("Argument '" + choiceArgs[0] + "' not recognised.");
        }
    }
}

let dbConstants = new constants.databaseConstants();
let dbOpsObj = new dbOps.databaseOperations(dbConstants);
let tOpsObj = new tOps.tableOperations(dbConstants);

runner();