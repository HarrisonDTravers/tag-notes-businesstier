var express = require('express');
var constants = require("../constants");
var tableOpsExport = require('../database/tableOperations');

var router = express.Router();
var dbConstants = new constants.databaseConstants();
var tableOps = new tableOpsExport.tableOperations(dbConstants);

/* GET users listing. */
router.get('/', async function(req, res, next) {
    let targetTable = dbConstants.tables[0]; //sets table
    let requestedSetName = req.get("setName");
    let setSelectResponse;
    try
    {
        setSelectResponse = await tableOps.selectAllWithIdFromTable(targetTable, ["setname", requestedSetName])
        try
        {
            setExists = setSelectResponse.rowCount == 1;
            if(setExists)
            {
                res.status(200).send();
            }
            else
            {
                res.status(400).send("Requested set does not exist.")
            }
        }
        catch(err)
        {
            console.log(setSelectResponse + " & err : " + err);
            res.status(500).send("Server failed when checking rowCount of selection query with row id (" + requestedSetName + ") from table (" + targetTable + ")");
        }
    }
    catch(err)
    {
        console.log(err);
        res.status(500).send("Server failed when selecting row with id (" + requestedSetName + ") from table (" + targetTable + ")");
    }
});

module.exports = router;