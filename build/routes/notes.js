var express = require('express');
var constants = require("../constants");
var tableOpsExport = require('../database/tableOperations');

var router = express.Router();
var dbConstants = new constants.databaseConstants();
var tableOps = new tableOpsExport.tableOperations(dbConstants);

/* GET users listing. */
router.get('/', async function(req, res, next) {
    let targetTable = dbConstants.tables[1];
    let requestedSetName = req.get("setName");
    try
    {
        notes = await tableOps.selectAllWithIdFromTable("notes", ["setname", requestedSetName]);        
        let tagsOnNotes;
        for(let ii=0; ii<notes.rowCount; ii++)
        {
            notes.rows[ii].tags = [];
            let tagsOnNote = await tableOps.selectAllWithIdFromTable("tagsonnotes", ["noteid", notes.rows[ii].noteid]);               
            for (let jj=0; jj<tagsOnNote.rowCount; jj++)
            {
                tableOps.selectAllWithIdFromTable("tags", ["tagid", tagsOnNote.rows[jj].tagid]).then((tag) => {
                    notes.rows[ii].tags.push(tag.rows[0]);
                }); //this select will only return 1 item
            }
        }
        //console.log("SELECT * on table (" + requestedSetName + ") returned " + notes.rowCount + " rows.");
        res.status(200);
        res.json(notes.rows);
        //console.log(notes.rows[29].tags);
    }
    catch(err)
    {
        console.log(err);
        res.status(500).send("Server failed when selecting row with id (" + requestedSetName + ") from table (" + targetTable + ")");
    }
});

router.put('/', async function(req, res, next) {
    let targetTable = dbConstants.tables[1];
    let noteData = req.get("noteData");
    let dataType = req.get("dataType");
    let setName = req.get("setName");
    try
    {
        notes = await tableOps.insertIntoTable(targetTable, [noteData, dataType, setName]);        

        if(notes.rowCount == 1)
        {
            res.status(200);
            res.json(notes.rows[0]);
        }
        else
        {
            console.log(res);
            throw new Error("Unable to get row from result when inserting note into database.", res)
        }
    }
    catch(err)
    {
        console.log(err);
        res.status(500).send("Server failed when selecting row with id (" + setName + ") from table (" + targetTable + ")");
    }
})

router.delete('/', async function(req, res, next) {
    let targetTable = dbConstants.tables[1];
    let noteId = req.get("noteId");
    try
    {
        notes = await tableOps.deleteFromTable(targetTable, [noteId]);        
        console.log(notes);

        if(notes.rowCount == 1)
        {
            res.status(200);
        }
        else
        {
            console.log(res);
            res.status(200).send("Unable to delete note with id : " + noteId + " as it doesn't exist.");
        }
    }
    catch(err)
    {
        console.log(err);
        res.status(500).send("Server failed when selecting row with id (" + setName + ") from table (" + targetTable + ")");
    }
})

module.exports = router;