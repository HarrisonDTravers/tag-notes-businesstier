class DatabaseConstants
    {
        constructor()
        {
            this.user = `postgres`;
            this.host = `127.0.0.1`;
            this.database = `postgres`;
            this.password = `postgres`;
            this.port = `5432`;             
            this.tables = ["sets", "notes", "tags", "tagsonnotes"];
            this.tablesCreationSQL = {
                "sets" : `CREATE TABLE public."sets"(setname character varying(32) COLLATE pg_catalog."default" NOT NULL, creationdate timestamp default current_timestamp, CONSTRAINT "sets_pkey" PRIMARY KEY (setname)) WITH (OIDS = FALSE) TABLESPACE pg_default; ALTER TABLE public."sets" OWNER to postgres`,
                "notes" : `CREATE TABLE public."notes"( noteid serial NOT NULL, notedata character varying(512) NOT NULL, datatype character varying(16) NOT NULL, creationdate timestamp default current_timestamp, setname character varying(32) NOT NULL, CONSTRAINT "notes_pkey" PRIMARY KEY (noteid), CONSTRAINT setname_fkey FOREIGN KEY (setname) REFERENCES public."sets" (setname) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION ) WITH ( OIDS = FALSE ) TABLESPACE pg_default; ALTER TABLE public."notes" OWNER to postgres;`,
                "tags" : `CREATE TABLE public."tags"( tagid serial NOT NULL, tagname character varying(16) COLLATE pg_catalog."default" NOT NULL, creationdate timestamp without time zone DEFAULT CURRENT_TIMESTAMP, setname character varying(32) COLLATE pg_catalog."default" NOT NULL, supertagid integer, CONSTRAINT tags_pkey PRIMARY KEY (tagid), CONSTRAINT setname_fkey FOREIGN KEY (setname) REFERENCES public.sets (setname) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION, CONSTRAINT supertagid_fkey FOREIGN KEY (supertagid) REFERENCES public.tags (tagid) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION ) WITH ( OIDS = FALSE ) TABLESPACE pg_default; ALTER TABLE public.tags OWNER to postgres;`,
                "tagsonnotes" : `CREATE TABLE public."tagsonnotes" ( tagid integer NOT NULL, noteid integer NOT NULL, CONSTRAINT "tagsonnotes_pkey" PRIMARY KEY (tagid, noteid), CONSTRAINT noteid_fkey FOREIGN KEY (noteid) REFERENCES public."notes" (noteid) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE, CONSTRAINT tagid_fkey FOREIGN KEY (tagid) REFERENCES public."tags" (tagid) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE CASCADE ) WITH ( OIDS = FALSE ) TABLESPACE pg_default; ALTER TABLE public."tagsonnotes" OWNER to postgres;`
            };
            this.tablesInsertionSQL = {
                "sets" : `INSERT INTO public.sets (setname) VALUES ('{0}');`,
                "notes" : `INSERT INTO public.notes (noteData, dataType, setname) VALUES ('{0}', '{1}', '{2}') RETURNING (noteid, creationdate);`,
                "tags" : `INSERT INTO public.tags (tagname, supertagid, setname) VALUES ('{0}', {1}, '{2}');`,
                "tagsonnotes" : `INSERT INTO public.tagsonnotes (tagid, noteid) VALUES ('{0}', '{1}');`
            };
            this.tablesSelectByColumnSQL = {
                "sets" : `SELECT * FROM public.sets WHERE {0} = '{1}';`,
                "notes" : `SELECT * FROM public.notes WHERE {0} = '{1}' ORDER BY noteid DESC;`,
                "tags" : `SELECT * FROM public.tags WHERE {0} = '{1}';`,
                "tagsonnotes" : `SELECT * FROM public.tagsonnotes WHERE {0} = '{1}';`,
            };
            this.tablesUpdateSQL = {
                "sets" : `UPDATE public.sets SET setname = '{0}' WHERE setname = '{1}';`,
                "notes" : `UPDATE public.notes SET noteData = '{0}', dataType = '{1}', setname = '{2}' WHERE noteid = '{3}';`,
                "tags" : `UPDATE public.tags SET tagname = '{0}', supertagid = '{1}', setname = '{2}' WHERE tagid = '{3}';`,
                "tagsonnotes" : `UPDATE public.tags SET tagid = '{0}', noteid = '{1}' WHERE tagid = '{2}' AND noteid = '{3}';`
            };
            this.tablesDeleteSQL = {
                "sets" : `DELETE FROM public.sets WHERE setname = '{0}';`,
                "notes" : `DELETE FROM public.notes WHERE noteid = '{0}';`,
                "tags" : `DELETE FROM public.tags WHERE tagid = '{0}';`,
                "tagsonnotes" : `DELETE FROM public.tagsonnotes WHERE tagid = '{0}' AND noteid = '{1}';`
            };
        }
    }

module.exports = {
    databaseConstants: DatabaseConstants
}
